FROM mcr.microsoft.com/dotnet/framework/sdk:4.8-windowsservercore-ltsc2019 AS build
WORKDIR /app

# copy everything else and build app
COPY src/. ./aspnetapp/
WORKDIR /app/aspnetapp
RUN msbuild /t:Build /p:Configuration=Release /p:DeployonBuild=True /p:DeployDefaultTarget=WebPublish /p:WebPublishMethod=FileSystem /p:DeleteExistingFiles=True /p:publishUrl=/app/published

FROM mcr.microsoft.com/dotnet/framework/aspnet:4.8-windowsservercore-ltsc2019 AS app
WORKDIR C:/inetpub/wwwroot
# copy published web application to app container
COPY --from=build /app/published/ ./
# RUN icacls App_Data /grant 'Everyone:(OI)(CI)(F)'
# EXPOSE 80
